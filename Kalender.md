# Kalender
## Erstellungszeitpunkt eines Kalendereintrags abrufen

1. https://calendar.google.com/calendar/ aufrufen.


2. Oben rechts unter ![](pics/set1.png): **Einstellungen**  auswählen.
  *Es öffnet sich die Seite https://calendar.google.com/calendar/r/settings*

3. Links den gewünschten **Kalender auswählen** (z.B. "Feiertage in Deutschland").
  *Auf der Seite erscheinen eine Reihe von "Karten" mit verschiedenen Einstellungsmöglichkeiten.*

4. Auf der Karte mit dem Titel "*Kalender Integrieren*" die Öffentlichs bzw. Private **Adresse im iCal-Format markieren** und mit **Strg+C** kopieren.
  ![](pics/cards1.png)

5. https://icalfilter.com aufrufen und die **iCal Addresse** in des Feld "*Url*" **einfügen**. Captcha lösen und mit *Go* bestätigen

6. "*Select All*" Auswählen und die Kalenderdaten mit **Strg+C** kopieren.

7. **Excel öffnen**, Zelle *A1* auswählen und Kalenderdaten mit **Strg+V** einfügen.
  ![](pics/ical.png)

  - Jede Zeile enthält einen Kalendereintrag.
  - In Spalte *SUMMARY* ist der **Titel** des Eintrags angegeben.
  - In Spalte *CREATED* ist der **Erstellungszeitpunkt** angegeben.
  - In Spalte *LAST-MODIFIED* ist der **Zeitpunkt der letzten Änderung** angegeben.
  - Die Spalten *DTSTART* bzw. *DTEND* geben **Anfang und Ende** des Termins an.
  - **Datenformat**
    - Die Zeitpunkte sind im YYYYMMDDHHMMSS Format angegeben (**Y**ear, **M**onth, **D**ay, **H**our, **M**inute, **S**econd)
      - Die ersten 4 Ziffern geben das Jahr an, die nächsten 2 Ziffern den Monat usw.
    - Beispiel: *20171224T201500Z* würde sich *2017 12 24 20 15 00* lesen und den Zeitpunkt am **24.12.2017** um **20:15:00** Uhr bezeichnen
    
8. Einen bestimmten **Eintrag finden**
    - In Excel eine beliebige Zelle auswählen und **Strg+F** drücken
    - Den *Titel* des gewünschten Eintrages eingeben und mit Enter bestätigen
    - Wenn das Suchwort in der Spalte SUMMARY gefunden wird, und die Zeitpunkte in *DTSTART* und *DTEND* zum gesuchten Kalendereintrag passen, handelt es sich bei der Zeile um den gesuchten Eintrag.
      - Wenn nicht: nochmal Enter drücken um den nächsten Treffer der Suche anzuzeigen. Wiederholen, bis der richtige Eintrag gefunden wurde
9. **Erstellungszeitpunkt** und **Zeitpunkt der letzten Änderung** können für den gesuchten Kalendereintrag abgelesen werden.
